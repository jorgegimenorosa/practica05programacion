/*Tras solucionar el problema de koby, el joven que le echo valor a su capitana pirata y dejo su banda, elo y luffy se van a la ciudad m�s pr�xima a reclutar
 * nuevos nakama para su tripulaci�n, entre los que se destaca el gran cazador de piratas Roronoa Zoro, que se encuentra encerrado en la base de la
 * armada de la ciudad. Cuando luffy se encuentra con Zoro, le pide que se una a su tripulaci�n, pero este se niega, debido a que su orgullo se lo impide */

package programa;

import java.util.Scanner;
import clases.GestorLibros;

public class Programa {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		GestorLibros miGestor = new GestorLibros();
		int opcion = 0;
		String mas = "";
		String titulo;
		String autor;
		String editorial;
		String nombre;
		String codigoSocio;
		int numeroCarnet = 0;
		double precio;
		do {
			System.out.println("Elije una opci�n (introduce 14 para salir)");
			System.out.println("1.- Dar de alta un libro");
			System.out.println("2.- Dar de alta un socio");
			System.out.println("3.- Listar los libros");
			System.out.println("4.- Listar los libros prestados por t�tulo");
			System.out.println("5.- Buscar un libro por t�tulo");
			System.out.println("6.- Eliminar un libro");
			System.out.println("7.- Listar los socios");
			System.out.println("8.- Eliminar un socio");
			System.out.println("9.- Buscar un socio por su c�digo de socio");
			System.out.println("10.- Comprobar si un libro est� bajo pr�stamo a un socio");
			System.out.println("11.- Buscar el socio con la mayor�a de pr�stamos");
			System.out.println("12.- Prestar un libro a un socio");
			System.out.println("13.- Comprobar cuanto tiempo queda hasta la devolucion de un prestamo");
			System.out.println("14.- Salir");
			opcion = input.nextInt();
			input.nextLine();
			switch (opcion) {
			case 1:
				do {
					System.out.println("Introduce los datos del libro");
					System.out.println("Introduce t�tulo");
					titulo = input.nextLine();
					System.out.println("Introduce autor");
					autor = input.nextLine();
					System.out.println("Introduce editorial");
					editorial = input.nextLine();
					System.out.println("Introduce precio");
					precio = input.nextDouble();
					input.nextLine();
					miGestor.altaLibro(titulo, autor, editorial, precio);
					System.out.println(
							"�Quieres introducir mas libros (introduce si para confirmar, introduce cualquier otra cadena de texto para salir)?");
					mas = input.nextLine();
				} while (mas.contains("si") == true);
				mas = "";
				break;
			case 2:
				do {
					System.out.println("Introduce los datos del socio");
					System.out.println("Introduce nombre");
					nombre = input.nextLine();
					System.out.println("Introduce codigoSocio");
					codigoSocio = input.nextLine();
					numeroCarnet++;
					miGestor.altaSocio(nombre, codigoSocio, numeroCarnet);
					System.out.println(
							"�Quieres introducir mas socios (introduce 'si' para confirmar, introduce cualquier otra cadena de texto para salir)?");
					mas = input.nextLine();
				} while (!mas.contains("si"));
				break;
			case 3:
				miGestor.listarLibros();
				break;
			case 4:
				System.out.println("Introduce el titulo del libro");
				titulo = input.nextLine();
				miGestor.listarLibroPrestado(titulo);
				break;
			case 5:
				System.out.println("Introduce el titulo del libro");
				titulo = input.nextLine();
				titulo.toLowerCase();
				miGestor.buscarLibro(titulo);
				break;
			case 6:
				System.out.println("Introduce el titulo del libro");
				titulo = input.nextLine();
				titulo.toLowerCase();
				miGestor.eliminarLibro(titulo);
				break;
			case 7:
				miGestor.listarSocios();
				break;
			case 8:
				System.out.println("Introduce el codigo del socio");
				codigoSocio = input.nextLine();
				miGestor.eliminarSocio(codigoSocio);
				break;
			case 9:
				System.out.println("Introduce el codigo del socio");
				codigoSocio = input.nextLine();
				miGestor.buscarSocio(codigoSocio);
				break;
			case 10:
				System.out.println("Introduce el titulo del libro");
				titulo = input.nextLine();
				titulo.toLowerCase();
				miGestor.listarLibroPrestado(titulo);
				break;
			case 11:
				miGestor.buscarSocioMayoriaPrestamos();
				break;
			case 12:
				System.out.println("Introduce el titulo del libro");
				titulo = input.nextLine();
				titulo.toLowerCase();
				System.out.println("Introduce el codigo del socio");
				codigoSocio = input.nextLine();
				miGestor.prestarLibroASocio(titulo, codigoSocio);
				break;
			case 13:
				System.out.println("Introduce el titulo del libro");
				titulo = input.nextLine();
				titulo.toLowerCase();
				miGestor.comprobarTiempoHastaDevolucion(titulo);
				break;
			case 14:
				System.out.println("Fin del programa");
				System.exit(0);
				break;
			default:
				System.out.println("Introduce una opci�n de las contempladas anteriormente");
			}
		} while (opcion != 14);
		input.close();
	}

}
