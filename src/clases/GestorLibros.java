/* El encargado de la base, Morgan mano de hacha, ordena la ejecuci�n del cazador de piratas, por lo que tras un breve enfrentamiento entre el y luffy,
 * sus hombres se dirijen a cumplir la orden de su superior. Por su parte, luffy se dirige hacia el lugar donde tienen las katanas de Zoro, para liberarlo
 * y que acceda a ser su nakama. */

package clases;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;

public class GestorLibros {
	private ArrayList<Libro> listaLibros;
	private ArrayList<Socio> listaSocios;

	public GestorLibros() {
		listaLibros = new ArrayList<Libro>();
		listaSocios = new ArrayList<Socio>();
	}

	public void altaLibro(String titulo, String autor, String editorial, double precio) {
		titulo.toLowerCase();
		Libro libro = new Libro(titulo, autor, editorial, precio);
		listaLibros.add(libro);
	}

	public void listarLibros() {
		for (Libro libro : listaLibros) {
			if (libro != null) {
				System.out.println(libro);
			}
		}
	}

	public Libro buscarLibro(String titulo) {
		for (int i = 0; i < listaLibros.size(); i++) {
			if (listaLibros.get(i) != null && listaLibros.get(i).getTitulo().equals(titulo)) {
				System.out.println(listaLibros.get(i));
			}
		}
		return null;
	}

	public void eliminarLibro(String titulo) {
		Iterator<Libro> iteradorLibro = listaLibros.iterator();

		while (iteradorLibro.hasNext()) {
			Libro libro = iteradorLibro.next();
			if (libro.getTitulo().equals(titulo)) {
				iteradorLibro.remove();
			}
		}
	}

	public void altaSocio(String nombre, String codigoSocio, int numeroCarnet) {
		Socio nuevoSocio = new Socio(nombre, codigoSocio);
		nuevoSocio.setFechaAlta(LocalDate.now());
		nuevoSocio.setNumeroCarnet(numeroCarnet);
		listaSocios.add(nuevoSocio);
	}

	public void eliminarSocio(String codigoSocio) {
		for (Socio socio : listaSocios) {
			if (socio.getCodigoSocio().equals(codigoSocio) && socio != null) {
				socio = null;
			}
		}
	}

	public Socio buscarSocio(String codigoSocio) {
		for (Socio socio : listaSocios) {
			if (socio != null && socio.getCodigoSocio().equals(codigoSocio)) {
				return socio;
			}
		}
		return null;
	}

	public Libro listarLibroPrestado(String titulo) {
		for (Libro libro : listaLibros) {
			if (libro != null && libro.getTitulo().equals(titulo)) {
				if (libro.getFechaPrestamo() != null) {
					return libro;
				}
			}
		}
		return null;
	}

	public Socio buscarSocioMayoriaPrestamos() {
		int maximo = 0;
		for (Socio socio : listaSocios) {
			if (socio.getNumeroPrestamos() > maximo) {
				maximo = socio.getNumeroPrestamos();
			}
		}
		for (Socio socio : listaSocios) {
			if (socio.getNumeroPrestamos() == maximo) {
				return socio;
			}
		}
		return null;
	}

	public void prestarLibroASocio(String titulo, String codigoSocio) {
		Libro libroAPrestar = buscarLibro(titulo);
		Socio socioConPrestamo = buscarSocio(codigoSocio);
		if (socioConPrestamo != null && libroAPrestar != null) {
			socioConPrestamo.setNumeroPrestamos(socioConPrestamo.getNumeroPrestamos() + 1);
			libroAPrestar.setPrestamo(true);
			libroAPrestar.setFechaPrestamo(LocalDate.now());
		}
	}

	public void comprobarTiempoHastaDevolucion(String titulo) {
		Libro libroPrestado = buscarLibro(titulo);
		LocalDate tiempoInicial;
		LocalDate tiempoFinal;
		for (Libro libro : listaLibros) {
			if (libroPrestado.getTitulo().equals(titulo)) {
				tiempoInicial = libro.getFechaPrestamo();
				tiempoFinal = libro.getFechaPrestamo().plusDays(21);
				System.out.println("Tu fecha vence el " + tiempoFinal + ", por lo que te quedan "
						+ (tiempoFinal.getDayOfYear() - tiempoInicial.getDayOfYear()) + " d�as.");
			} else {
				System.out.println("No existe el libro");
			}
		}
	}

	public void listarSocios() {
		for (Socio socio : listaSocios) {
			if (socio != null) {
				System.out.println(socio);
			}
		}
	}
}
