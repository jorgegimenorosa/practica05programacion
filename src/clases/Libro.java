package clases;

import java.time.LocalDate;

public class Libro {
	private String titulo;
	private String autor;
	private String editorial;
	private double precio;
	private LocalDate fechaPrestamo;
	private Socio socioBiblioteca;
	private boolean prestamo;

	public Libro(String titulo, String autor, String editorial, double precio) {
		this.titulo = titulo;
		this.autor = autor;
		this.editorial = editorial;
		this.precio = precio;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getEditorial() {
		return editorial;
	}

	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public LocalDate getFechaPrestamo() {
		return fechaPrestamo;
	}

	public void setFechaPrestamo(LocalDate fechaPrestamo) {
		this.fechaPrestamo = fechaPrestamo;
	}

	public Socio getSocioBiblioteca() {
		return socioBiblioteca;
	}

	public void setSocioBiblioteca(Socio socioBiblioteca) {
		this.socioBiblioteca = socioBiblioteca;
	}

	public boolean isPrestamo() {
		return prestamo;
	}

	public void setPrestamo(boolean prestamo) {
		this.prestamo = prestamo;
	}

	@Override
	public String toString() {
		return "Libro [titulo=" + titulo + ", autor=" + autor + ", editorial=" + editorial + ", precio=" + precio
				+ ", fechaPrestamo=" + fechaPrestamo + ", socioBiblioteca=" + socioBiblioteca + ", prestamo=" + prestamo
				+ "]";
	}

}
