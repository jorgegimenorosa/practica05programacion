package clases;

import java.time.LocalDate;

public class Socio {
	private String nombre;
	private String codigoSocio;
	private int numeroCarnet;
	private LocalDate fechaAlta;
	private int numeroPrestamos;

	public Socio(String nombre, String codigoSocio) {
		this.nombre = nombre;
		this.codigoSocio = codigoSocio;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodigoSocio() {
		return codigoSocio;
	}

	public void setCodigoSocio(String codigoSocio) {
		this.codigoSocio = codigoSocio;
	}

	public int getNumeroCarnet() {
		return numeroCarnet;
	}

	public void setNumeroCarnet(int numeroCarnet) {
		this.numeroCarnet = numeroCarnet;
	}

	public LocalDate getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(LocalDate fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public int getNumeroPrestamos() {
		return numeroPrestamos;
	}

	public void setNumeroPrestamos(int numeroPrestamos) {
		this.numeroPrestamos = numeroPrestamos;
	}

	@Override
	public String toString() {
		return "Socio [nombre=" + nombre + ", codigoSocio=" + codigoSocio + ", numeroCarnet=" + numeroCarnet
				+ ", fechaAlta=" + fechaAlta + ", numeroPrestamos=" + numeroPrestamos + "]";
	}

}
